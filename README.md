# Shared Drafts

Share your unpublished drafts with expiring private links!

## Tools used
- [babeljs](https://babeljs.io) for ECMAScript 2015+ support
- [vue.js](https://vuejs.org) for componentizing code
- [Browserify](https://browserify.org) for bundling builds


(This [AJAX plugin](https://codex.wordpress.org/AJAX_in_Plugins) is a fork of Drafts for Friends, version 2.2).

## Dependencies

No dependencies are necessary to install this plugin into your WordPress instance. However, if you would like to change and persist JavaScript changes, the bundled JavaScript files must be rebuilt. The following is recommended:

- [node.js LTS](https://github.com/nodejs/LTS) (latest - version 6 at the time of writing)
- [yarn package management](https://yarnpkg.com/) (latest)

Run the following to install tooling necessary for JavaScript builds:

``` bash
# Option A: install dependencies using yarn (fast)
yarn install

# Option B: install dependencies using NPM (slow)
npm install
```

## Production Builds

The following will bundle the JavaScript assets to the `./dist` folder in a production-ready state. In other words, this will perform minification, mapping, and debug code pruning.

``` bash
# build for production with minification
npm run build
```

## Development Builds

The following will watch for changes to JavaScript assets and continuously bundle them to the `./dist` folder. This will not perform any code optimizations.

``` bash
# watch for changes to JavaScript assets and rebundle as necessary
npm run develop
```

## Todo's in no particular order
- Add ability to create/manage permanent, un-expiring share links
- Add the ability to sort the dashboard table by columns
- Add ability to add arbitrary text notes for each share
- Integrate analytics to study user behavior
- Add responsive styling to dashboard table to accommodate narrower viewports.
- Improve error handling both on the server and the client
- Handle internationalization using [vue-i18n](https://github.com/kazupon/vue-i18n)
- Introduce a testing framework for PHP
- Introduce a testing framework for JavaScript
