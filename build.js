const babelify = require("babelify")
const browserify = require('browserify')
const envify = require('envify/custom')
const fs = require('fs')
const vueify = require('vueify')
const watchify = require('watchify')
const path = require('path')
const exorcist = require('exorcist')

const config = {
  input: path.join(__dirname, './js/main.js'),
  output: path.join(__dirname, './dist/main.js'),
  outputMap: path.join(__dirname, './dist/main.js.map'),
  isProduction: process.env.NODE_ENV === 'production',
}

// Watch iff building for development
const watchOptions = config.isProduction ? {
  debug: true,
} : {
  cache: {},
  debug: true,
  packageCache: {},
  plugin: [ watchify ],
};

// Attach necessary configurations and transformations to browserify instance
let instance = browserify( watchOptions )
  .transform( envify( {
    NODE_ENV: ! config.isProduction ? 'development' : 'production',
  } ), {
    global: true,
  } )
  .transform('browserify-css', {
    autoInject: true,
    global: true,
    minify: config.isProduction,
  } )
  .transform( babelify )
  .transform( vueify );

// Attach additional minification transformation to production builds
if ( config.isProduction ) {
  instance = instance
    .transform( 'uglifyify', {
      global: true,
    } );
}

// Require the entry file
instance = instance.require( config.input, { entry: true } );

if ( ! config.isProduction ) {
  // These events are only available if watchify is enabled
  instance.on( 'update', bundle );
  instance.on( 'log', onComplete );
}

instance.on( 'error', onError );
bundle();

function bundle() {
  console.log( `${new Date()} | [Build] Bundling assets...` );
  const bundledInstance = instance
    .bundle()
    .on( 'error', onError );
  if ( config.isProduction ) {
    bundledInstance
      .pipe( exorcist( config.outputMap ) )
      .pipe( fs.createWriteStream( config.output ) );
  } else {
    bundledInstance
      .pipe( fs.createWriteStream( config.output ) );
  }
}

function onError( error ) {
  console.error( 'Error detected' );
  console.error( error.message );
}

function onComplete( message ) {
  console.log( `${new Date()} | [Build] Bundling complete - ${message}` );
}
