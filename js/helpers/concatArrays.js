export default function concatArrays (arrayA, arrayB) {
  if (Array.isArray(arrayA) && Array.isArray(arrayB)) {
    return arrayA.concat(arrayB)
  } else if (Array.isArray(arrayA)) {
    return arrayA
  } else if (Array.isArray(arrayB)) {
    return arrayB
  } else {
    return []
  }
}
