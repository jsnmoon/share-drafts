export const actions = {
  create: 'shared_drafts_create_share',
  delete: 'shared_drafts_delete_share',
  deleteMultiple: 'shared_drafts_delete_shares',
  modify: 'shared_drafts_modify_share',
  readMultiple: 'shared_drafts_read_shares',
}

export const urls = {
  ajax: window.WP_VARS.admin_ajax_url,
  posts: window.WP_VARS.admin_post_url,
}

export const nonces = {
  ...window.WP_VARS.nonces,
}

export const initialSharables = window.WP_VARS.sharables

export const defaultFlatpickrConfig = {
  altFormat: 'Y/m/d h:iK',
  altInput: true,
  enableTime: true,
}

export const momentDateFormat = 'YYYY/MM/DD hh:mma'
