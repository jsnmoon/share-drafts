export default function containsLengthyArrayAtKey(object, key) {
  return (
    typeof object === 'object' &&
    key in object &&
    Array.isArray(object[key]) &&
    object[key].length > 0
  )
}
