import getNonce from './getNonce'

export default function generateFormData(payload) {
  const data = new FormData()
  if (typeof payload === 'object' && 'action' in payload) {
    const keys = Object.keys(payload)
    if (keys.length > 0) {
      // Append all payload key-value pairs into form data object
      keys.map(key => {
        const value = payload[key]
        if (Array.isArray(value)) {
          value.map(v => {
            data.append(`${key}[]`, v)
          })
        } else {
          data.append(key, payload[key])
        }
      })

      // Append nonce to security field
      data.append('nonce', getNonce(payload.action))
    }
  }
  return data
}
