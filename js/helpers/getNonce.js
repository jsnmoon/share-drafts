import {
  nonces,
} from './constants'

export default function getNonce(action) {
  if (typeof action === 'string' && action in nonces) {
    return nonces[action]
  } else {
    return null
  }
}
