export default function handleAxiosError (error) {
  // TODO: Enhance error handling and render user-actionable error on UI

  if (error.response) {
    // The request was made, but the server responded with a non-2xx status code
    if (process.env.NODE_ENV !== 'production') {
      console.log('Non-2xx status code received')
      console.log(error.response.data);
      console.log(error.response.status);
      console.log(error.response.headers);
    }

    // Handle nonce verification failure caused by session expiration
    if (error.response.status === 403 && error.response.data === -1) {
      if (process.env.NODE_ENV !== 'production') {
        console.log('Nonce verification failed, refreshing window in two seconds...')
      }
      setTimeout(() => {
        window.location.reload()
      }, 2000)
    }
  } else {
    // Something happened in setting up the request that triggered an Error
    if (process.env.NODE_ENV !== 'production') {
      console.log('Axios Error:', error.message);
    }
    throw error
  }
}
