import 'babel-polyfill'
import Vue from 'vue'

import App from './App.vue'
import './styles'

if (process.env.NODE_ENV !== 'production') {
  console.log('WP_VARS', window.WP_VARS);
}

new Vue({
  ...App,
}).$mount('#shared-drafts')
