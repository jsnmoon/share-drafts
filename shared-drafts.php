<?php
/*
Plugin Name: Shared Drafts
Plugin URI: http://automattic.com/
Description: Share your unpublished drafts with expiring private links!
Author: Jason Moon
Version: 1.0.2
Author URI: https://github.com/jsnmoon
*/

class Shared_Drafts {

	protected $slug                  = 'shared-drafts';
	protected $version               = '1.0.2';
	protected $shared_post           = null;
	protected $shared_posts          = null;
	protected $shared_posts_for_user = null;

	function __construct(){
		add_action( 'init', array( $this, 'init' ));
	}

	function init() {
		add_action( 'admin_menu', array( $this, 'add_admin_pages' ));
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ) );

		add_action( 'wp_ajax_shared_drafts_read_shares', array( $this, 'ajax_read_shares' ) );
		add_action( 'wp_ajax_shared_drafts_create_share', array( $this, 'ajax_create_share' ) );
		add_action( 'wp_ajax_shared_drafts_modify_share', array( $this, 'ajax_modify_share' ) );
		add_action( 'wp_ajax_shared_drafts_delete_share', array( $this, 'ajax_delete_share' ) );
		add_action( 'wp_ajax_shared_drafts_delete_shares', array( $this, 'ajax_delete_shares' ) );

		add_filter( 'the_posts', array( $this, 'the_posts_intercept' ));
		add_filter( 'posts_results', array( $this, 'posts_results_intercept' ));

		$this->initialize_share_data();
	}

	/*
		Plugin data structure setup
	*/

	private function initialize_share_data() {
		global $current_user;

		$this->shared_posts = $this->get_shared_posts();

		if (
			isset( $current_user->data->ID ) &&
			$current_user->data->ID > 0 &&
			isset( $this->shared_posts[ $current_user->data->ID ] )
		) {
			$this->shared_posts_for_user = $this->shared_posts[ $current_user->data->ID ];
		} else {
			$this->shared_posts_for_user = array();
		}
		$this->save_shared_posts();
	}

	/*
		Admin menu link additions
	*/
	function add_admin_pages() {
		$page_title = __( 'Shared Drafts', 'shareddrafts' );
		$function = array( $this, 'output_dashboard_content' );
		add_submenu_page(
			'edit.php',
			$page_title,
			$page_title,
			'administrator',
			__FILE__,
			$function
		);
	}

	/*
		Asset loading and backend variable injection
	*/

	function enqueue_scripts( $hook_page ) {
		$resource_handle = $this->slug . '-main';

		// Check to see if the loaded page is shared-drafts.php
		if ( false === strpos( $hook_page, $this->slug ) ) {
			return;
		}

		wp_enqueue_script(
			$resource_handle,
			plugins_url( 'dist/main.js', __FILE__ ),
			array(),
			$this->version,
			true // Load JS in footer so that templates in DOM can be referenced.
		);

		wp_localize_script(
			$resource_handle,
			'WP_VARS',
			$this->generate_js_injectible_variables()
		);
	}

	private function generate_js_injectible_variables() {
		return array(
			'admin_ajax_url' => admin_url( 'admin-ajax.php' ),
			'admin_post_url' => admin_url( 'post.php' ),
			'sharables'      => $this->get_sharables(),
			'nonces'         => array(
				'shared_drafts_read_shares'   =>  wp_create_nonce( 'shared_drafts_read_shares' ),
				'shared_drafts_create_share'  =>  wp_create_nonce( 'shared_drafts_create_share' ),
				'shared_drafts_modify_share'  =>  wp_create_nonce( 'shared_drafts_modify_share' ),
				'shared_drafts_delete_share'  =>  wp_create_nonce( 'shared_drafts_delete_share' ),
				'shared_drafts_delete_shares' =>  wp_create_nonce( 'shared_drafts_delete_shares' ),
			),
		);
	}

	/*
		Data fetching and saving
	*/

	private function _get_posts( $post_type ) {
		global $current_user;
		global $wpdb;

		if ( ! in_array( $post_type, array( 'draft', 'future', 'pending' ) ) ) {
			return array();
		} else {
			$user_id = $current_user->data->ID;
			return $wpdb->get_results( $wpdb->prepare( "
				SELECT id, post_title as title FROM $wpdb->posts WHERE
				post_type = 'post' AND
				post_status = %s AND
				post_author = %d
				ORDER BY post_modified DESC
			", $post_type, $user_id ) );
		}
	}

	private function _get_posts_with_permalinks( $post_type ) {
		$output = array();
		foreach( $this->_get_posts( $post_type ) as $post ) {
			$output[] = array(
				'id'        => $post->id,
				'title'     => $post->title,
				'permalink' => get_permalink($post->id),
			);
		}
		return $output;
	}

	private function get_shared_posts() {
		$saved_options = get_option( 'shared' );
		return is_array( $saved_options ) ? $saved_options : array();
	}

	private function save_shared_posts(){
		global $current_user;
		if ( isset( $current_user->data->ID ) && $current_user->data->ID > 0 ) {
			$this->shared_posts[ $current_user->data->ID ] = $this->shared_posts_for_user;
		}
		update_option( 'shared', $this->shared_posts );
	}

	private function get_sharables() {
		$draft   = $this->_get_posts_with_permalinks( 'draft' );
		$pending = $this->_get_posts_with_permalinks( 'pending' );
		$future  = $this->_get_posts_with_permalinks( 'future' );
		$shared  = $this->shared_posts_for_user;
		return array(
			'draft'   => $draft,
			'future'  => $future,
			'pending' => $pending,
			'shared'  => $shared,
		);
	}

	/*
		AJAX Handlers
	*/

	private function _ajax_return( $response = true ) {
		echo json_encode( $response );
		wp_die();
	}

	function ajax_read_shares() {
		check_ajax_referer( 'shared_drafts_read_shares', 'nonce' );

		return $this->_ajax_return( $this->get_sharables() );
	}

	function ajax_create_share() {
		check_ajax_referer( 'shared_drafts_create_share', 'nonce' );

		$message = null;

		if ( isset( $_POST['post_id'] ) && isset( $_POST['expiration_date'] ) ) {
			// Sanitize user input
			$safe_post_id = intval( $_POST['post_id'] );
			$safe_expiration_date = intval( $_POST['expiration_date'] );

			$post = get_post( $safe_post_id );
			if ( ! $post ) {
				$message = __( 'There is no such post!', 'shareddrafts' );
			} elseif ( 'publish' === get_post_status( $post ) ) {
				$message = __( 'The post is already published!', 'shareddrafts' );
			} elseif ( ! $safe_post_id ) {
				$message = __( 'Invalid post ID received!', 'shareddrafts' );
			} elseif ( ! $safe_expiration_date ) {
				$message = __( 'Invalid expiration date received!', 'shareddrafts' );
			} else {
				$this->shared_posts_for_user[] = array(
					'expiration_date' => $safe_expiration_date,
					'id'              => $post->ID,
					'key'             => 'sd_' . wp_generate_password( 12, false ),
				);
				$this->save_shared_posts();
			}
		} else {
			$message = __( 'Invalid request received!', 'shareddrafts' );
		}

		return $this->_ajax_return( array(
			'message'   => $message,
			'sharables' => $this->get_sharables(),
		) );
	}

	function ajax_modify_share() {
		check_ajax_referer( 'shared_drafts_modify_share', 'nonce' );

		$shared = array();
		$message = null;
		$safe_expiration_date = intval( $_POST['expiration_date'] );

		if ( ! $safe_expiration_date ) {
			$message = __( 'Invalid expiration date received!', 'shareddrafts' );
		} else {
			foreach( $this->shared_posts_for_user as $share ) {
				if ( $share['key'] === $_POST['key'] ) {
					$share['expiration_date'] = $safe_expiration_date;
				}
				$shared[] = $share;
			}
			$this->shared_posts_for_user = $shared;
			$this->save_shared_posts();
			$message = __( 'Share updated!', 'shareddrafts' );
		}


		return $this->_ajax_return( array(
			'message'   => $message,
			'sharables' => $this->get_sharables(),
		) );
	}

	function ajax_delete_share() {
		check_ajax_referer( 'shared_drafts_delete_share', 'nonce' );

		$shared = array();
		foreach( $this->shared_posts_for_user as $share ) {
			if ( $share['key'] !== $_POST['key'] ) {
				$shared[] = $share;
			}
		}
		$this->shared_posts_for_user = $shared;
		$this->save_shared_posts();

		return $this->_ajax_return( array(
			'message'   => __( 'Share deleted!', 'shareddrafts' ),
			'sharables' => $this->get_sharables(),
		) );
	}

	function ajax_delete_shares() {
		check_ajax_referer( 'shared_drafts_delete_shares', 'nonce' );

		$shared = array();
		$delete_keys = $_POST['keys'];

		if ( is_array( $delete_keys ) ) {
			$safe_delete_keys = array_map( 'sanitize_text_field', $delete_keys );

			foreach( $this->shared_posts_for_user as $share ) {
				if ( ! in_array( $share['key'], $safe_delete_keys ) ) {
					$shared[] = $share;
				}
			}

			$this->shared_posts_for_user = $shared;
			$this->save_shared_posts();
		}

		return $this->_ajax_return( array(
			'delete_keys' => $delete_keys,
			'message'     => __( 'Shares deleted!', 'shareddrafts' ),
			'sharables'   => $this->get_sharables(),
		) );
	}

	/*
		Post filter handlers
	*/

	function can_view( $post_id ) {
		foreach( $this->shared_posts as $posts ) {
			foreach( $posts as $post ) {
				$match_ids = $post['id'] === $post_id;
				$match_keys = isset( $_GET['shared-drafts'] ) && $post['key'] === $_GET['shared-drafts'];
				$current_time_in_ms = time() * 1000;
				$unexpired = $post['expiration_date'] > $current_time_in_ms;
				if ( $match_ids && $match_keys && $unexpired ) {
					return true;
				}
			}
		}
		return false;
	}

	function posts_results_intercept( $posts ) {
		if ( 1 !== count( $posts ) ) {
			return $posts;
		} else {
			$post = $posts[0];
			$status = get_post_status( $post );
			if ( 'publish' !== $status && $this->can_view( $post->ID ) ) {
				$this->shared_post = $post;
			}
			return $posts;
		}
	}

	function the_posts_intercept( $posts ) {
		if ( empty( $posts ) && ! is_null( $this->shared_post ) ) {
			return array( $this->shared_post );
		} else {
			$this->shared_post = null;
			return $posts;
		}
	}

	/*
		HTML output
	*/
	function output_dashboard_content() {
		?>
		<!-- Main content - JS attachment point -->
		<div id="shared-drafts" />
		<?php
	}
}

new Shared_Drafts();
